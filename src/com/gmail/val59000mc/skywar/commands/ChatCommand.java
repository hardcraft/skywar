package com.gmail.val59000mc.skywar.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.skywar.players.PlayersManager;
import com.gmail.val59000mc.skywar.players.SPlayer;

public class ChatCommand implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			SPlayer gPlayer = PlayersManager.instance().getSPlayer((Player) sender);
			
			if(gPlayer != null){
				gPlayer.setGlobalChat(!gPlayer.isGlobalChat());
				gPlayer.sendI18nMessage("command.global-chat."+gPlayer.isGlobalChat());
				return true;
			}
		}
		return false;
	}

}
