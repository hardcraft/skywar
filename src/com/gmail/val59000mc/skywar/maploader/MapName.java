package com.gmail.val59000mc.skywar.maploader;

public enum MapName {
	skywar_ice,
	skywar_mushroom,
	skywar_grass,
	skywar_nether
}
