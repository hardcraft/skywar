package com.gmail.val59000mc.skywar.players;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.skywar.classes.PlayerClassManager;
import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.skywar.dependencies.VaultManager;
import com.gmail.val59000mc.skywar.game.EndCause;
import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.skywar.game.GameState;
import com.gmail.val59000mc.skywar.i18n.I18n;
import com.gmail.val59000mc.skywar.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.skywar.titles.TitleManager;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Texts;

public class PlayersManager {
	private static PlayersManager instance;
	
	private List<SPlayer> players;
	private List<STeam> teams;
	
	// static
	
	public static PlayersManager instance(){
		if(instance == null){
			instance = new PlayersManager();
		}
		
		return instance;
	}
	
	// constructor 
	
	private PlayersManager(){
		players = Collections.synchronizedList(new ArrayList<SPlayer>());
		teams = Collections.synchronizedList(new ArrayList<STeam>());
		
		teams.add(new STeam(TeamType.RED, Config.spawnsLocations.get(0)));
		teams.add(new STeam(TeamType.BLUE, Config.spawnsLocations.get(1)));
		teams.add(new STeam(TeamType.YELLOW, Config.spawnsLocations.get(2)));
		teams.add(new STeam(TeamType.GOLD, Config.spawnsLocations.get(3)));
		teams.add(new STeam(TeamType.GREEN, Config.spawnsLocations.get(4)));
		teams.add(new STeam(TeamType.DARK_PURPLE, Config.spawnsLocations.get(5)));
		teams.add(new STeam(TeamType.LIGHT_PURPLE, Config.spawnsLocations.get(6)));
		teams.add(new STeam(TeamType.AQUA, Config.spawnsLocations.get(7)));
	}
	
	
	// Accessors
	
	public SPlayer getSPlayer(Player player){
		return getSPlayer(player.getName());
	}
	
	public SPlayer getSPlayer(String name){
		for(SPlayer gPlayer : getPlayers()){
			if(gPlayer.getName().equals(name))
				return gPlayer;
		}
		
		return null;
	}
	
	public synchronized List<SPlayer> getPlayers(){
		return players;
	}
	
	public synchronized List<STeam> getTeams(){
		return teams;
	}

	public synchronized STeam getTeam(TeamType type){
		for(STeam team : getTeams()){
			if(team.getType().equals(type)){
				return team;
			}
		}
		return null;
	}
	
	public boolean canAddPlayerToTeam(STeam evoTeam) {
		return evoTeam.getMembers().size() < getMinMembersTeam().getMembers().size()+1;
	}
	
	private STeam getMinMembersTeam(){
		STeam min = getTeams().get(0);
		for(STeam team : getTeams()){
			if(team.getMembers().size() < min.getMembers().size()){
				min = team;
			}
		}
		return min;
	}
	
	private STeam getMaxNotFullTeam(){
		STeam max = null;
		for(STeam team : getTeams()){
			if(!team.isFull()){
				if(max == null){
					max = team;
				}else{
					if(team.getMembers().size() > max.getMembers().size()){
						max = team;
					}
				}
			}
		}
		return max;
	}

	public List<STeam> getPlayingTeams() {

		List<STeam> playingTeams = new ArrayList<STeam>();
		for(STeam slaveTeam : getTeams()){
			if(slaveTeam.isPlaying()){
				playingTeams.add(slaveTeam);
			}
		}
		
		return playingTeams;
	}
	
	
	// Methods 
	
	public synchronized SPlayer addPlayer(Player player){
		SPlayer newPlayer = new SPlayer(player);
		getPlayers().add(newPlayer);
		return newPlayer;
	}
	
	public synchronized void removePlayer(Player player){
		removePlayer(player.getName());
	}
	
	public synchronized void removePlayer(String name){
		SPlayer gPlayer = getSPlayer(name);
		if(gPlayer != null){
			gPlayer.leaveTeam();
			getPlayers().remove(gPlayer);
		}
	}
	
	public boolean isPlaying(Player player){
		SPlayer gPlayer = getSPlayer(player);
		if(gPlayer != null){
			return gPlayer.getState().equals(PlayerState.PLAYING);
		}
		return false;
	}

	public boolean isPlayerAllowedToJoin(Player player){
		Logger.debug("-> PlayersManager::isPlayerAllowedToJoin, player="+player.getName());
		GameManager gm = GameManager.instance();
		
		switch(gm.getState()){
				
			case WAITING:
			case PLAYING:
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=true, gameState="+gm.getState());
				return true;
			case STARTING:
			case LOADING:
			case ENDED:
			default:
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=false, gameState="+gm.getState());
				return false;
		}
	}

	public void playerJoinsTheGame(Player player) {
		Logger.debug("-> PlayersManager::playerJoinsTheGame, player="+player.getName());
		SPlayer gPlayer = getSPlayer(player);
		
		if(gPlayer == null){
			gPlayer = addPlayer(player);
		}
		
		GameState gameState = GameManager.instance().getState();
		switch(gameState){
			case WAITING:
				gPlayer.setState(PlayerState.WAITING);
				break;
			case PLAYING:
			case STARTING:
			case LOADING:
			case ENDED:
			default:
				gPlayer.setState(PlayerState.DEAD);
				break;
		}
			
		switch(gPlayer.getState()){
			case WAITING:
				Logger.debug("waitPlayer");
				gPlayer.sendI18nMessage("player.welcome");
				Logger.broadcast(
					I18n.get("player.joined")
						.replace("%player%",gPlayer.getName())
						.replace("%count%", String.valueOf(getPlayers().size()))
						.replace("%total%",  String.valueOf(Config.maxPlayers))
				);
				if(Config.isBountifulApiLoaded){
					TitleManager.sendTitle(player, ChatColor.GREEN+"Sky War", 20, 20, 20);
				}
				waitPlayer(gPlayer);
				break;
			case DEAD:
			default:
				Logger.debug("spectatePlayer");
				spectatePlayer(gPlayer);
				break;
		}

		
		gPlayer.refreshNameTag();
		refreshVisiblePlayers();
	}

	public void waitPlayer(SPlayer gPlayer){
		
		if(gPlayer.isOnline()){
			if(Bukkit.getOnlinePlayers().size()>Config.maxPlayers){
				gPlayer.sendI18nMessage("player.full");
			}
			Player player = gPlayer.getPlayer();
			player.teleport(Config.lobby);
			player.setGameMode(GameMode.ADVENTURE);
			Effects.addPermanent(player, PotionEffectType.SATURATION, 0);
			Effects.addPermanent(player, PotionEffectType.SPEED, 1);
			player.setHealth(20);

			if(Config.isSIGLoaded){
				SIG.getPlugin().getAPI().giveInventoryToPlayer(player, "join");
			}
		}
		
	}
	
	public void startPlayer(final SPlayer uPlayer){
		uPlayer.setState(PlayerState.PLAYING);
		uPlayer.teleportToSpawnPoint();
		UpdateScoreboardThread.add(uPlayer);
		uPlayer.setGlobalChat(false);
		if(uPlayer.isOnline()){
			Player player = uPlayer.getPlayer();
			player.setGameMode(GameMode.SURVIVAL);
			player.setFireTicks(0);
			Sounds.play(player, Sound.ENDERDRAGON_GROWL, 2, 2);
			Effects.clear(player);
			PlayerClassManager.instance().giveClassItems(uPlayer);
		}
	}
	
	public void spectatePlayer(SPlayer gPlayer){

		gPlayer.setState(PlayerState.DEAD);
		gPlayer.sendI18nMessage("player.spectate");
		
		if(gPlayer.isOnline()){
			Player player = gPlayer.getPlayer();
			Inventories.clear(player);
			player.setGameMode(GameMode.SPECTATOR);
			player.teleport(Config.lobby);
		}
	}
	
	public List<SPlayer> getPlayingPlayers() {
		List<SPlayer> playingPlayers = new ArrayList<SPlayer>();
		for(SPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.PLAYING) && p.isOnline()){
				playingPlayers.add(p);
			}
		}
		return playingPlayers;
	}
	
	public List<SPlayer> getWaitingPlayers() {
		List<SPlayer> waitingPlayers = new ArrayList<SPlayer>();
		for(SPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.WAITING) && p.isOnline()){
				waitingPlayers.add(p);
			}
		}
		return waitingPlayers;
	}

	public void startAllPlayers() {
		Logger.debug("-> PlayersManager::startAllPlayers");
		assignRandomTeamsToPlayers();
		refreshVisiblePlayers();
		for(SPlayer gPlayer : getPlayers()){
			if(gPlayer.getTeam() == null){
				spectatePlayer(gPlayer);
			}else{
				startPlayer(gPlayer);
			}
		}
		UpdateScoreboardThread.start();
		Logger.debug("<- PlayersManager::startAllPlayers");
	}
	
	private void refreshVisiblePlayers() {
		for(Player player : Bukkit.getOnlinePlayers()){
			for(Player onePlayer : Bukkit.getOnlinePlayers()){
				player.hidePlayer(onePlayer);
				player.showPlayer(onePlayer);
			}
		}
	}
	
	private void assignRandomTeamsToPlayers(){
		Logger.debug("-> PlayersManager::assignRandomTeamsToPlayers");
		
		synchronized(players){
			// Shuffle players who will play (up to maxPlayers)
			List<SPlayer> playersWhoWillPlay = new ArrayList<SPlayer>(getWaitingPlayers().subList(0, Config.maxPlayers > players.size() ? players.size() : Config.maxPlayers));
			List<SPlayer> playersWhoWillNotPlay = new ArrayList<SPlayer>();
			if(players.size() > Config.maxPlayers){
				playersWhoWillNotPlay = new ArrayList<SPlayer>(getWaitingPlayers().subList(Config.maxPlayers, players.size()));	
			}
			this.players = new ArrayList<SPlayer>();
			Collections.shuffle(playersWhoWillPlay);
			this.players.addAll(playersWhoWillPlay);
			this.players.addAll(playersWhoWillNotPlay);

			for(SPlayer sPlayer : playersWhoWillNotPlay){
				sPlayer.leaveTeam();
			}
		}
		
		// assign two players by team
		
		int nPlayer = 0;
		for(SPlayer sPlayer : getWaitingPlayers()){
			nPlayer++;
			
			if(nPlayer <= Config.maxPlayers){
				
				STeam team = sPlayer.getTeam();
				
				if(sPlayer.getTeam() == null){
					team = getMaxNotFullTeam();
					team.addPlayer(sPlayer);
				}
				
			}
			
			sPlayer.refreshNameTag();
		}

		Logger.debug("<- PlayersManager::assignRandomTeamsToPlayers");
	}

	public void endAllPlayers(EndCause cause, STeam winningTeam) {
		
			if(winningTeam != null){
			for(SPlayer gPlayer : winningTeam.getMembers()){
				if(gPlayer.isOnline()){
					rewardWin(gPlayer);
				}
			}
		}

		for(SPlayer gPlayer : getPlayers()){
			gPlayer.setGlobalChat(true);
			if(gPlayer.isOnline()){
				spectatePlayer(gPlayer);
				printEndMessage(gPlayer, winningTeam);
			}
		}

		Sounds.playAll(Sound.ENDERDRAGON_GROWL,0.8f,2);
		
	}

	private void rewardWin(SPlayer gPlayer) {
		if(gPlayer.isOnline()){
			Player player = gPlayer.getPlayer();
			double money = VaultManager.addMoney(player, Config.winReward);
			gPlayer.addMoney(money);
			player.sendMessage("§a+"+money+" §fHC");
			Sounds.play(player, Sound.LEVEL_UP, 1, 2);
		}
	}
	
	private void printEndMessage(SPlayer rPlayer, STeam winningTeam){
		if(rPlayer.isOnline() && winningTeam != null){
			Player player = rPlayer.getPlayer();
			Texts.tellraw(player, I18n.get("stats.end",player)
					.replace("%kills%", String.valueOf(rPlayer.getKills()))
					.replace("%hardcoins%", String.valueOf(rPlayer.getMoney()))
					.replace("%winner%", winningTeam.getColor()+winningTeam.getI18nName(player))
			);
		}
	}
	
	
	
}
