package com.gmail.val59000mc.skywar.players;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.skywar.dependencies.PermissionsExManager;
import com.gmail.val59000mc.skywar.i18n.I18n;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Numbers;

import ca.wacos.nametagedit.NametagAPI;

public class SPlayer {
	private String name;
	private STeam team;
	private PlayerState state;
	private boolean globalChat;
	private int kills;
	private double money;
	private Location spawnpoint;
	
	
	// Constructor
	
	public SPlayer(Player player){
		this.name = player.getName();
		this.team = null;
		this.state = PlayerState.WAITING;
		this.globalChat = true;
		this.kills = 0;
		this.money = 0;
		this.spawnpoint = Config.lobby;
	}
	
	
	
	// Accessors

	public String getName() {
		return name;
	}
	public Location getSpawnPoint() {
		return spawnpoint;
	}
	public void setSpawnPoint(Location location) {
		this.spawnpoint = location;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public synchronized STeam getTeam() {
		return team;
	}
	public synchronized void setTeam(STeam team) {
		this.team = team;
	}
	public PlayerState getState() {
		return state;
	}
	public void setState(PlayerState state) {
		this.state = state;
	}
	public double getMoney() {
		return Numbers.round(money,2);
	}
	public void addMoney(double money) {
		this.money += money;
	}
	public boolean isGlobalChat() {
		return globalChat;
	}
	public void setGlobalChat(boolean globalChat) {
		this.globalChat = globalChat;
	}
	public int getKills() {
		return kills;
	}
	public void addKill() {
		this.kills++;
	}
	
	// Methods


	public Player getPlayer(){
		return Bukkit.getPlayer(name);
	}
	
	public Boolean isOnline(){
		return Bukkit.getPlayer(name) != null;
	}
	
	public boolean isInTeamWith(SPlayer player){
		return (team != null && team.equals(player.getTeam()));
	}
	
	public void leaveTeam(){
		if(team != null){
			team.leave(this);
			team = null;
		}
	}

	public void sendI18nMessage(String code) {
		if(isOnline()){
			Logger.sendMessage(getPlayer(), I18n.get(code,getPlayer()));
		}
	}
	
	
	public String toString(){
		return "[name='"+getName()+"']";
	}

	public void teleportToSpawnPoint() {
		if(isOnline() && getSpawnPoint() != null){
			getPlayer().teleport(getSpawnPoint());
		}
	}

	public boolean isState(PlayerState state) {
		return getState().equals(state);
	}
	
	public void refreshNameTag(){
		String prefix = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', PermissionsExManager.getShortPrefix(getPlayer())));

		
		String suffix = "&f";
		
		if(getTeam() != null){
			switch(getTeam().getType()){
				case BLUE:
					suffix = "&9";
					break;
				case RED:
					suffix = "&c";
					break;
				case DARK_PURPLE:
					suffix = "&5";
					break;
				case GOLD:
					suffix = "&6";
					break;
				case AQUA:
					suffix = "&b";
					break;
				case GREEN:
					suffix = "&a";
					break;
				case LIGHT_PURPLE:
					suffix = "&d";
					break;
				case YELLOW:
					suffix = "&e";
					break;
				default:
					break;
			}
			
		}
		
		NametagAPI.setPrefix(getName(), "&7"+prefix+suffix);
	}



	public ChatColor getColor(){
		if(getTeam() != null){
			return getTeam().getColor();
		}else{
			return ChatColor.WHITE;
		}
	}


}
