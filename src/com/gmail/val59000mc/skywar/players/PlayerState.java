package com.gmail.val59000mc.skywar.players;

public enum PlayerState {
  WAITING,
  PLAYING,
  DEAD;
}
