package com.gmail.val59000mc.skywar.players;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.skywar.i18n.I18n;

public class STeam {

	private List<SPlayer> members;
	private Location spawnPoint;
	private TeamType type;
	
	public STeam(TeamType type, Location spawnPoint) {
		this.members = new ArrayList<SPlayer>();
		this.spawnPoint = spawnPoint;
		this.type = type;
	}

	public void sendI18nMessage(String code) {
		for(SPlayer wicPlayer: members){
			wicPlayer.sendI18nMessage(code);
		}
	}
	
	public String getI18nName(Player player){
		return I18n.get("team."+getType().toString(), player);
	}
	
	public boolean contains(SPlayer player){
		return members.contains(player);
	}
	
	public synchronized List<SPlayer> getMembers(){
		return members;
	}


	public TeamType getType() {
		return type;
	}

	public List<SPlayer> getPlayingMembers(){
		List<SPlayer> playingMembers = new ArrayList<SPlayer>();
		for(SPlayer uhcPlayer : getMembers()){
			if(uhcPlayer.getState().equals(PlayerState.PLAYING)){
				playingMembers.add(uhcPlayer);
			}
		}
		return playingMembers;
	}
	
	public synchronized List<String> getMembersNames(){
		List<String> names = new ArrayList<String>();
		for(SPlayer player : getMembers()){
			names.add(player.getName());
		}
		return names;
	}
	
	public void addPlayer(SPlayer gPlayer){
		gPlayer.leaveTeam();
		gPlayer.setTeam(this);
		getMembers().add(gPlayer);
		gPlayer.setSpawnPoint(this.spawnPoint);
	}

	public boolean isFull(){
		return getMembers().size() >= Config.playersPerTeam;
	}
	
	public void leave(SPlayer gPlayer){
		getMembers().remove(gPlayer);
		gPlayer.setSpawnPoint(Config.lobby);
	}
	
	public boolean isOnline(){
		for(SPlayer gPlayer : getMembers()){
			if(gPlayer.isOnline()){
				return true;
			}
		}
		return false;
	}


	public boolean isPlaying() {
		for(SPlayer gPlayer : getMembers()){
			if(gPlayer.isOnline() && gPlayer.isState(PlayerState.PLAYING)){
				return true;
			}
		}
		return false;
	}
	
	public List<SPlayer> getOtherMembers(SPlayer excludedPlayer){
		List<SPlayer> otherMembers = new ArrayList<SPlayer>();
		for(SPlayer uhcPlayer : getMembers()){
			if(!uhcPlayer.equals(excludedPlayer))
				otherMembers.add(uhcPlayer);
		}
		return otherMembers;
	}
	
	public Boolean is(TeamType type) {
		return getType().equals(type);
	}

	public ChatColor getColor() {
		return getType().getColor();
	}

	public boolean canAddPlayer() {
		if(getMembers().size() < Config.playersToStart/2){
			return true;
		}else{
			return PlayersManager.instance().canAddPlayerToTeam(this);
		}
	}

}
