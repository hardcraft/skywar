package com.gmail.val59000mc.skywar.players;

import org.bukkit.ChatColor;

public enum TeamType {
	RED(ChatColor.RED),
	BLUE(ChatColor.BLUE),
	YELLOW(ChatColor.YELLOW),
	GOLD(ChatColor.GOLD),
	GREEN(ChatColor.GREEN),
	DARK_PURPLE(ChatColor.DARK_PURPLE),
	LIGHT_PURPLE(ChatColor.LIGHT_PURPLE),
	AQUA(ChatColor.AQUA);

	private ChatColor color;

	private TeamType(ChatColor color){
		this.color = color;
	}
	
	public ChatColor getColor() {
		return color;
	}
}
