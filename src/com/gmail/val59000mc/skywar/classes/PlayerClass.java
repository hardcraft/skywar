package com.gmail.val59000mc.skywar.classes;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.skywar.players.SPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;

public class PlayerClass {
	protected List<ItemStack> items;
	protected List<ItemStack> armor;
	
	public PlayerClass(
			List<ItemStack> items, 
			List<ItemStack> armor) {
		super();
		this.items = items;
		this.armor = armor;
	}
	
	public void giveClassItems(SPlayer uPlayer) {
		if(uPlayer.isOnline()){
			Player player = uPlayer.getPlayer();
			Inventories.clear(player);
			Inventories.setArmor(player, armor);
			Inventories.give(player, items);
		}
	}
	
}
