package com.gmail.val59000mc.skywar.classes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.skywar.players.SPlayer;

public class PlayerClassManager {
	
	
	private static PlayerClassManager instance;
	private PlayerClass stuff;

	public static PlayerClassManager instance(){
		if(instance == null){
			instance = new PlayerClassManager();
		}
		
		return instance;
	}

	private PlayerClassManager(){
	}
	
	public static void load(){
		
		PlayerClassManager instance = PlayerClassManager.instance();

		// items
		List<ItemStack> items = new ArrayList<ItemStack>();
		items.add(new ItemStack(Material.WOOD_SWORD, 1));
		items.add(new ItemStack(Material.WOOD_AXE, 1));
		items.add(new ItemStack(Material.WOOD_PICKAXE, 1));
		items.add(new ItemStack(Material.COOKED_BEEF, 2));
		
		
		// Red
		List<ItemStack> armor = new ArrayList<ItemStack>();
		armor.add(new ItemStack(Material.LEATHER_HELMET, 1));
		armor.add(new ItemStack(Material.LEATHER_CHESTPLATE, 1));
		armor.add(new ItemStack(Material.LEATHER_LEGGINGS, 1));
		armor.add(new ItemStack(Material.LEATHER_BOOTS, 1));
		
		instance.stuff = new PlayerClass(items,armor);
	
	}
	
	public void giveClassItems(SPlayer uPlayer) {
		instance.stuff.giveClassItems(uPlayer);
	}
}
