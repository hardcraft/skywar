package com.gmail.val59000mc.skywar.i18n;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;

public class English {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();

		// config
		s.put("config.dependency.vault-not-found", "Plugin Vault not found, no support for rewards");
		s.put("config.dependency.vault-loaded", "Plugin Vault loaded.");
		s.put("config.dependency.worldedit-not-found", "Plugin WorldEdit not found, no support for schematics");
		s.put("config.dependency.worldedit-loaded", "Plugin WorldEdit loaded.");
		s.put("config.dependency.libsdisguise-not-found", "Plugin LibsDisguises not found, no support for disguises");
		s.put("config.dependency.libsdisguise-loaded", "Plugin LibsDisguises loaded.");
		s.put("config.dependency.effectlib-not-found", "Plugin EffectLib not found, no support for effects");
		s.put("config.dependency.effectlib-loaded", "Plugin EffectLib loaded.");
		s.put("config.dependency.bountifulapi-not-found", "Plugin BountifulAPI not found, no support for titles");
		s.put("config.dependency.bountifulapi-loaded", "Plugin BountifulAPI loaded.");
		
		// commands
		s.put("command.global-chat.true", "§aYou are now talking to everyone.");
		s.put("command.global-chat.false", "§aYou are now talking to your team.");
		s.put("command.start.not-possible", "§cYou can't force starting the game.");
		s.put("command.start.ok", "§aGame starting has been forced.");
		
		
	    // parser
		s.put("parser.wrong-location", "Couldn't parser the location %location%");
		
		s.put("parser.item.not-found", "Couldn't find item string");
		s.put("parser.item.empty-string", "Item string is empty");
		s.put("parser.item.wrong-material", "The material name doesn't exist");
		s.put("parser.item.wrong-damage-value", "Wrong damage value");
		s.put("parser.item.wrong-amount", "Wrong amount");
		s.put("parser.item.wrong-enchantment-syntax", "Wrong enchantment syntax");
		s.put("parser.item.wrong-enchantment-name", "Wrong enchantment name");
		s.put("parser.item.wrong-enchantment-level", "Wrong enchantment level");
		
		s.put("parser.playerclass.not-found", "Player class not found");
		s.put("parser.playerclass.items-not-found", "items not found in player class");
		s.put("parser.playerclass.wrong-player-class", "Cannot instantiate player class");
		
					
		// map loader
		s.put("map-loader.load.no-last-world", "No last world to load, creating a new world");
		s.put("map-loader.load.last-world-not-found", "Last world not found, creating a new world.");
		s.put("map-loader.delete.no-last-world", "Not last world to delete.");
		s.put("map-loader.delete.last-world-not-found", "Last world to delete not found.");
		s.put("map-loader.delete", "Deleting last world.");
		s.put("map-loader.copy.not-found", "'winteriscoming' directory not found, cannot copy.");

		// game
		s.put("game.player-allowed-to-join", "Players are now allowed to join");
		s.put("game.start", "The game begins !");
		s.put("game.starting-in", "The game will begin in %time% !");
		s.put("game.end-in", "The game will end in %time ! because there are not enough players left.");
		s.put("game.end", "The game is finished !");
		s.put("game.end-stopped", "The game continues !");
		s.put("game.shutting-down-in", "Shutting down in %time% !");
		s.put("game.remaining-time", "Time left : %time% !");
		s.put("game.end.team-win", ChatColor.GREEN+"The game is finished ! Winners : ");
		s.put("game.end.no-more-players", ChatColor.GREEN+"Game has ended because there were no more players left.");
		s.put("game.leave-arena", ChatColor.RED+"Don't try to leave the map.");
		s.put("game.refill-chests-warning", ChatColor.GOLD+"Refillings chests in 10 seconds !");
		s.put("game.refill-chests", ChatColor.GREEN+"Chests have been refilled !");
		
		s.put("stats.end", "[\"\",{text:\"]-----[ Sky War game summary ]-----[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Kills : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  HardCoins earned : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"  >>  Winning team : \",bold:true,color:green},{text:\"%winner%\n\n\"},{text:\"]----------------------------------------[\",color:green}]");
		s.put("stats.death", "[\"\",{text:\"]-----[ You are dead ]-----[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Kills : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  HardCoins earned : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"\n\n\"},{text:\"]----------------------------------------[\",color:green}]");

		// players
		s.put("player.not-allowed-to-join", "You are not allowed to join that game");
		s.put("player.welcome", "Welcome in Sky War game !");
		s.put("player.joined", "§f%player% §ajoined the game §2§l[%count%/%total%]");
		s.put("player.full", "The game if full. If nobody logs out you will be a spectator !");
		s.put("player.spectate", "You are spectating.");
		s.put("player.gladiator", ChatColor.GREEN+"You are a gladiator");
		s.put("player.slave", ChatColor.GREEN+"You are a slave");
		s.put("player.coins-earned", ChatColor.GREEN+"HardCoins earned : ");
		s.put("player.died", "%player% §rdied");
		s.put("player.killed", "%killer% §rkilled %killed%");
		
		// items
		s.put("items.regen-head-lore", "§aRight click to regen your team.");
		s.put("items.regenerating-team", "§aTeam regeneration for 5 seconds !");
		
		// team
		s.put("team.RED", "Red");
		s.put("team.BLUE", "Blue");
		s.put("team.YELLOW", "Yellow");
		s.put("team.GOLD", "Orange");
		s.put("team.GREEN", "Green");
		s.put("team.DARK_PURPLE", "Purple");
		s.put("team.LIGHT_PURPLE", "Pink");
		s.put("team.AQUA", "Cyan");
		s.put("team.full", "§cThere are already too many players in that team.");
		
		
		// scoreboard
		s.put("scoreboard.team", "Team");
		s.put("scoreboard.kills", "Kills");
		s.put("scoreboard.player-class", "Class");
		s.put("scoreboard.coins-earned", "Coins");
		s.put("scoreboard.teammates", "Teammates");
		
		s.put("scoreboard.online-players", "Players");
		s.put("scoreboard.time-to-start", "Starting in");

		// ping
		s.put("ping.loading", "Loading");
		s.put("ping.playing", "Playing");
		s.put("ping.starting", "Starting");
		s.put("ping.waiting", "Waiting");
		s.put("ping.ended", "Ended");
				
				
		return s;
	}

}
