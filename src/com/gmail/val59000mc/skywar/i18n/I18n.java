package com.gmail.val59000mc.skywar.i18n;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.entity.Player;

public class I18n {
	
	private static Map<Language,Map<String,String>> strings;
	private static Map<UUID,Language> playersLanguages;
	
	// class initialize
	static{
		strings = new HashMap<Language,Map<String,String>>();
		strings.put(Language.ENGLISH, English.load());
		strings.put(Language.FRENCH, French.load());
		
		playersLanguages = new HashMap<UUID,Language>();
	}
	
	/**
	 * Get a plugin string in the player's language
	 * @param code
	 * @param p
	 * @return the translated plugin message
	 */
	public static String get(String code, Player p){
		Language l = getLanguage(p);
		String message = strings.get(l).get(code);
		return ((message == null) ? code.toUpperCase() : message);
	}
	

	/**
	 * Get a plugin string in the default language
	 * @param code
	 * @return a plugin message in the default language
	 */
	public static String get(String code) {
		return get(code,null);
	}
	
	/**
	 * Get the player language, should default to ENGLISH if it doens't exist
	 * @param p
	 * @return
	 */
	private static Language getLanguage(Player p){
		if(p == null)
			return Language.FRENCH;
		
		Language l = playersLanguages.get(p.getUniqueId());
		if(l == null){
			l = Language.FRENCH;
			playersLanguages.put(p.getUniqueId(), l);
		}

		return l;
	}
}
