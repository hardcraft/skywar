package com.gmail.val59000mc.skywar;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.spigotutils.Logger;

public class SkyWar extends JavaPlugin{
	
	private static SkyWar pl;
	
	
	public void onEnable(){
		pl = this;
	
		// Blocks players joins while loading the plugin
		Bukkit.getServer().setWhitelist(true);
		saveDefaultConfig();
		
		Logger.setColoredPrefix(ChatColor.WHITE+"["+ChatColor.GREEN+"SkyWar"+ChatColor.WHITE+"]"+ChatColor.RESET+" ");
		Logger.setStrippedPrefix("[SkyWar] ");
		
		Bukkit.getScheduler().runTaskLater(this, new Runnable(){
			
			public void run() {
				GameManager.instance().loadGame();
				
				// Unlock players joins and rely on UhcPlayerJoinListener
				Bukkit.getServer().setWhitelist(false);
			}
			
		}, 1);
		
		
	}
	
	public static SkyWar getPlugin(){
		return pl;
	}
	
	public void onDisable(){
	}
}
