package com.gmail.val59000mc.skywar.chests;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.spigotutils.Blocks;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class ChestManager {
	private Set<BlockState> centerChests;
	private Set<BlockState> spawnsChests;
	private Set<BlockState> lobbyBlocks;
	
	public ChestManager(){
		this.centerChests = Sets.newHashSet();
		this.spawnsChests = Sets.newHashSet();
		this.lobbyBlocks = Sets.newHashSet();
	}
	
	public void load(){
		Logger.debug("-> ChestManager::load");
		

		Logger.debug("Looking for center chests around "+Locations.printLocation(Config.center)+" in a radius of "+Config.centerRadius);
		centerChests = Blocks.findNearbyBlockStates(Config.center, Config.centerRadius, Sets.newHashSet(Material.CHEST));
		for(BlockState centerChest : centerChests){
			Logger.debug("Center chest found at "+Locations.printLocation(centerChest.getLocation()));
		}

		for(Location spawn : Config.spawnsLocations){
			Logger.debug("Looking for spawn chests around "+Locations.printLocation(spawn)+" in a radius of "+Config.spawnsRadius);
			spawnsChests.addAll(Blocks.findNearbyBlockStates(spawn, Config.spawnsRadius, Sets.newHashSet(Material.CHEST)));
		}
		for(BlockState spawnChest : spawnsChests){
			Logger.debug("Spawn chest found at "+Locations.printLocation(spawnChest.getLocation()));
		}

		Logger.debug("Looking for lobby blocks around "+Locations.printLocation(Config.lobby)+" in a radius of "+Config.lobbyRadius);
		lobbyBlocks = Blocks.findNearbyBlockStates(Config.lobby, Config.lobbyRadius, null);
		

		Logger.debug("<- ChestManager::load, spawnChests="+spawnsChests.size()+", centerChests="+centerChests.size()+", lobbyBlocks="+lobbyBlocks.size());
	}
	
	public void eraseLobby(){
		for(BlockState block : lobbyBlocks){
			if(!block.getType().equals(Material.AIR)){
				block.setType(Material.AIR);
				block.update(true);
			}
		}
	}
	
	public void refillChests(){
		
		Logger.debug("Refilling all chests");

		for(BlockState block : centerChests){
			Logger.debug("Refilling center chest at "+Locations.printLocation(block.getLocation()));
			block.getBlock().setType(Material.CHEST);
			Chest chest = (Chest) block.getBlock().getState();
			chest.getInventory().clear();

			List<ItemStack> itemsToAdd = Lists.newArrayList();
			itemsToAdd.addAll(getRandomItems(Config.spawnsItems, 4));
			itemsToAdd.add(new ItemStack(Material.STONE,5));
			itemsToAdd.addAll(getRandomItems(Config.centerItems, 5));
			
			for(ItemStack item : itemsToAdd){
				Logger.debug("Adding item "+item.getType());
				chest.getInventory().addItem(item);
			}
			chest.update(true);
		}
		
		for(BlockState block : spawnsChests){
			Logger.debug("Refilling spawn chest at "+Locations.printLocation(block.getLocation()));
			block.getBlock().setType(Material.CHEST);
			Chest chest = (Chest) block.getBlock().getState();
			chest.getInventory().clear();
			
			List<ItemStack> itemsToAdd = Lists.newArrayList();
			itemsToAdd.addAll(getRandomItems(Config.spawnsItems, 6));
			itemsToAdd.add(new ItemStack(Material.STONE,5));
			itemsToAdd.addAll(getRandomItems(Config.centerItems, 3));
			
			for(ItemStack item : itemsToAdd){
				Logger.debug("Adding item "+item.getType());
				chest.getInventory().addItem(item);
			}
			chest.update(true);
		}
		
		
	}

	private List<ItemStack> getRandomItems(List<ItemStack> itemsRef, int amount) {
		List<ItemStack> items = Lists.newArrayList();
		int maxSize = itemsRef.size();
		if(maxSize > 0){
			for(int i=0 ; i<amount ; i++){
				ItemStack toAdd = itemsRef.get(Randoms.randomInteger(0, maxSize-1)).clone();
				if(toAdd.getType().equals(Material.BOW)){
					items.add(new ItemStack(Material.ARROW,5));
				}
				items.add(toAdd);
			}
		}
		
		return items;
	}
}
