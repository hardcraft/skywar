package com.gmail.val59000mc.skywar.threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import com.gmail.val59000mc.skywar.SkyWar;
import com.gmail.val59000mc.skywar.i18n.I18n;
import com.gmail.val59000mc.skywar.players.PlayerState;
import com.gmail.val59000mc.skywar.players.SPlayer;
import com.gmail.val59000mc.spigotutils.SimpleScoreboard;

public class UpdateScoreboardThread implements Runnable{
	

	private static UpdateScoreboardThread instance;
	private Map<SPlayer,SimpleScoreboard> scoreboards;
	


	public static void start(){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		Bukkit.getScheduler().runTaskAsynchronously(SkyWar.getPlugin(), instance);
	}
	
	public static void add(SPlayer gPlayer){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		instance.addGPlayer(gPlayer);
	}
	
	private void addGPlayer(SPlayer gPlayer){
		if(!scoreboards.containsKey(gPlayer)){
			SimpleScoreboard sc = new SimpleScoreboard("Sky War");
			if(gPlayer.isOnline()){
				setupTeamColors(sc,gPlayer);
			}
			getScoreboards().put(gPlayer, sc);
		}
	}
	
	private void setupTeamColors(SimpleScoreboard sc, SPlayer gPlayer) {
		
		Scoreboard scoreboard = sc.getBukkitScoreboard();
		Objective kills = scoreboard.registerNewObjective("kills", "playerKillCount");
		kills.setDisplaySlot(DisplaySlot.PLAYER_LIST);
		
	}
	
	private UpdateScoreboardThread(){
		this.scoreboards = Collections.synchronizedMap(new HashMap<SPlayer,SimpleScoreboard>());
	}
	
	private synchronized Map<SPlayer,SimpleScoreboard> getScoreboards(){
		return scoreboards;
	}
	
	@Override
	public void run() {
		Bukkit.getScheduler().runTask(SkyWar.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				
						
					for(Entry<SPlayer,SimpleScoreboard> entry : getScoreboards().entrySet()){
						SPlayer gPlayer = entry.getKey();
						SimpleScoreboard scoreboard = entry.getValue();
						
						if(gPlayer.isOnline() && gPlayer.getTeam() != null){

							Player player = gPlayer.getPlayer();
							
							List<String> content = new ArrayList<String>();
							content.add(" ");
							
							// Kills / Deaths
							content.add(I18n.get("scoreboard.kills",player));
							content.add(" "+ChatColor.GREEN+""+gPlayer.getKills()+ChatColor.WHITE);
							
							// Coins
							content.add(I18n.get("scoreboard.coins-earned",player));
							content.add(" "+ChatColor.GREEN+""+gPlayer.getMoney());
							
							// Teammate
							content.add(I18n.get("scoreboard.teammates",player));
							for(SPlayer gTeamMate : gPlayer.getTeam().getMembers()){
								if(gTeamMate.isState(PlayerState.PLAYING)){
									content.add(" "+gPlayer.getColor()+""+gTeamMate.getName());
								}else{
									content.add(" "+gPlayer.getColor()+""+gTeamMate.getName());
								}
							}	
							
							scoreboard.clear();
							for(String line : content){
								scoreboard.add(line);
							}
							scoreboard.draw();
							scoreboard.send(player);
						}
					}

					Bukkit.getScheduler().runTaskLaterAsynchronously(SkyWar.getPlugin(), instance, 20);
				}
				
			});
				
		
	}
	
	
	
	
}
