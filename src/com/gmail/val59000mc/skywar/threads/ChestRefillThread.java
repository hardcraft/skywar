package com.gmail.val59000mc.skywar.threads;

import org.bukkit.Bukkit;
import org.bukkit.Sound;

import com.gmail.val59000mc.skywar.SkyWar;
import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.skywar.i18n.I18n;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;


public class ChestRefillThread implements Runnable{

	private static ChestRefillThread instance;
	
	private boolean run;
	
	public static void start(){
		Logger.debug("-> ChestRefillThread::start");
		if(instance == null){
			instance = new ChestRefillThread();
			Bukkit.getScheduler().runTaskLaterAsynchronously(SkyWar.getPlugin(), instance, Config.refillChestsDelay*20);
		}
		Logger.debug("<- ChestRefillThread::start");
	}



	public static void stop() {
		instance.run = false;
	}
	
	
	public ChestRefillThread(){
		this.run = true;
	}
	
	public void run() {
		
		Bukkit.getScheduler().runTask(SkyWar.getPlugin(), new Runnable(){

			public void run(){
				if(run){

					refillChests();
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(SkyWar.getPlugin(), ChestRefillThread.this, Config.refillChestsDelay*20);
				}
			}

			private void refillChests() {
				
				Logger.broadcast(I18n.get("game.refill-chests-warning"));
				Sounds.playAll(Sound.PISTON_EXTEND, 2, 2);
				
				Bukkit.getScheduler().runTaskLater(SkyWar.getPlugin(), new Runnable() {
					
					public void run() {

						Logger.broadcast(I18n.get("game.refill-chests"));
						Sounds.playAll(Sound.PISTON_EXTEND, 2, 2);
						GameManager.instance().getChestManager().refillChests();
					}
				}, 200);
				
			}
			
		});

	}
}
