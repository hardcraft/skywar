package com.gmail.val59000mc.skywar.threads;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.skywar.SkyWar;
import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.skywar.i18n.I18n;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;

public class WaitForNewPlayersThread implements Runnable{

	private static WaitForNewPlayersThread instance;
	
	private int remainingTime;
	private boolean willStart;
	private SimpleScoreboard scoreboard;
	
	
	public static void start(){
		Logger.debug("-> WaitForNewPlayersThread::start");
		if(instance == null){
			Bukkit.getScheduler().runTaskAsynchronously(SkyWar.getPlugin(), new WaitForNewPlayersThread());
		}
		Logger.debug("<- WaitForNewPlayersThread::start");
	}

	public static boolean force() {
		if(instance != null){
			instance.willStart = true;
			instance.remainingTime = 10;
			return true;
		}
		return false;
	}
	
	public WaitForNewPlayersThread(){
		instance = this;
		this.remainingTime = Config.countdownToStart;
		this.willStart = false;
		this.scoreboard = new SimpleScoreboard("Sky War");
	}
	
	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(SkyWar.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				int onlinePlayers = Bukkit.getOnlinePlayers().size();
				
				// update scoreboard
				updateScoreboard();
				
				if(willStart || onlinePlayers >= Config.playersToStart){
					willStart = true;
					if(remainingTime <= 0){
						GameManager.instance().startGame();
						return;
					}
					if(remainingTime <= 10 || (remainingTime > 0 && remainingTime%10 == 0 ) || remainingTime == Config.countdownToStart){
						Logger.broadcast(I18n.get("game.starting-in").replace("%time%", Time.getFormattedTime(remainingTime)));
						Sounds.playAll(Sound.NOTE_PIANO);
					}
					remainingTime--;
				}else{
					instance.remainingTime = Config.countdownToStart;
				}
				
				
				Bukkit.getScheduler().runTaskLaterAsynchronously(SkyWar.getPlugin(), instance, 20);
			
			}
		
		});

	}
	
	private void updateScoreboard(){
		
		List<String> content = new ArrayList<String>();
		content.add(" ");
		
		// Team
		content.add(I18n.get("scoreboard.online-players"));
		content.add(" "+ChatColor.GREEN+Bukkit.getOnlinePlayers().size()+" / "+Config.maxPlayers);
		
		if(remainingTime != Config.countdownToStart){
			// Kills / Deaths
			content.add(I18n.get("scoreboard.time-to-start"));
			content.add(" "+ChatColor.GREEN+""+Time.getFormattedTime(remainingTime));
		}
		
		
		scoreboard.clear();
		for(String line : content){
			scoreboard.add(line);
		}
		scoreboard.draw();
		
		for(Player player : Bukkit.getOnlinePlayers()){
			scoreboard.send(player);
		}
		
		
	}
	
}