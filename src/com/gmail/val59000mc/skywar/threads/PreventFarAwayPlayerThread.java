package com.gmail.val59000mc.skywar.threads;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.skywar.SkyWar;
import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.skywar.game.GameState;
import com.gmail.val59000mc.skywar.players.PlayerState;
import com.gmail.val59000mc.skywar.players.PlayersManager;
import com.gmail.val59000mc.skywar.players.SPlayer;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;

public class PreventFarAwayPlayerThread implements Runnable {
	
	private static PreventFarAwayPlayerThread thread;
	private boolean run;
	
	public static void start(){
		Logger.debug("-> KillFarAwayPlayerThread::start");
		PreventFarAwayPlayerThread thread = new PreventFarAwayPlayerThread();
		Bukkit.getScheduler().runTaskAsynchronously(SkyWar.getPlugin(), thread);
		Logger.debug("<- KillFarAwayPlayerThread::start");
	}
	
	private PreventFarAwayPlayerThread() {
		thread = this;
		this.run = true;
	}
	
	public static void stop() {
		thread.run = false;
	}

	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(SkyWar.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				if(run){
					
					LocationBounds bounds = Config.bounds;
					
					
					if(bounds != null && bounds.getWorld() != null){

						World world = bounds.getWorld();
						
						for(Player player : Bukkit.getOnlinePlayers()){

							SPlayer wicPlayer = PlayersManager.instance().getSPlayer(player);
							
							if(wicPlayer != null && player.getWorld().equals(world)){
								if(!bounds.contains(player.getLocation())){
									if( !GameManager.instance().isState(GameState.PLAYING) || player.getLocation().getY() >= 0){
										player.teleport(Config.lobby);
										wicPlayer.sendI18nMessage("game.leave-arena");
									}else if(GameManager.instance().isState(GameState.PLAYING) 
											&& wicPlayer.isState(PlayerState.PLAYING)
											&& wicPlayer.isOnline()
											&& player.getLocation().getY() < 0 
											&& player.getHealth() > 0){
										player.setHealth(0);
										Sounds.play(player, Sound.HURT_FLESH, 1, 1);
									}
								}
								
							}
						}
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(SkyWar.getPlugin(), thread, 30);
				}
				
				
			}
			
		});
		
	}

}
