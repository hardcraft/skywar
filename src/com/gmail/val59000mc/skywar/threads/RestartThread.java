package com.gmail.val59000mc.skywar.threads;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.skywar.SkyWar;
import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.skywar.i18n.I18n;
import com.gmail.val59000mc.spigotutils.Bungee;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Time;

public class RestartThread implements Runnable{

	private static RestartThread instance;
	
	RestartThread thread;
	long remainingTime;
	
	
	public static void start(){
		Logger.debug("-> RestartThread::start");
		if(instance == null){
			Bukkit.getScheduler().runTaskAsynchronously(SkyWar.getPlugin(), new RestartThread());
		}
		Logger.debug("<- RestartThread::start");
	}
	
	
	public RestartThread(){
		instance = this;
		this.remainingTime = 15;
	}
	
	@Override
	public void run(){
		
		Bukkit.getScheduler().runTask(SkyWar.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				remainingTime--;
				
				if(remainingTime == 5){
					if(Config.isBungeeEnabled){
						for(Player player : Bukkit.getOnlinePlayers()){
							Bungee.sendPlayerToServer(SkyWar.getPlugin(),player,Config.bungeeServer);
						}
					}
				}
				
				if(remainingTime <= 0){
					
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "save-all");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "stop");
				}else{
					Bukkit.getScheduler().runTaskLaterAsynchronously(SkyWar.getPlugin(), instance, 20);
				}
			}
		});		
		
	}

}
