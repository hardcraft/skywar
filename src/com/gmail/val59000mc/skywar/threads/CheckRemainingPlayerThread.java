package com.gmail.val59000mc.skywar.threads;

import java.util.List;

import org.bukkit.Bukkit;

import com.gmail.val59000mc.skywar.SkyWar;
import com.gmail.val59000mc.skywar.game.EndCause;
import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.skywar.players.PlayersManager;
import com.gmail.val59000mc.skywar.players.STeam;
import com.gmail.val59000mc.spigotutils.Logger;


public class CheckRemainingPlayerThread implements Runnable{

	private static CheckRemainingPlayerThread instance;
	
	private boolean run;
	
	public static void start(){
		Logger.debug("-> CheckRemainingPlayerThread::start");
		CheckRemainingPlayerThread thread = new CheckRemainingPlayerThread();
		Bukkit.getScheduler().runTaskAsynchronously(SkyWar.getPlugin(), thread);
		Logger.debug("<- CheckRemainingPlayerThread::start");
	}



	public static void stop() {
		instance.run = false;
	}
	
	
	public CheckRemainingPlayerThread(){
		instance = this;
		this.run = true;
	}
	
	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(SkyWar.getPlugin(), new Runnable(){

			@Override
			public void run(){
				if(run){
					List<STeam> playingTeams = PlayersManager.instance().getPlayingTeams();
					if(playingTeams.size() == 1){
						GameManager.instance().endGame(EndCause.TEAM_WIN,playingTeams.get(0));
					}
					if(playingTeams.size() == 0){
						GameManager.instance().endGame(EndCause.NO_MORE_PLAYERS,null);
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(SkyWar.getPlugin(), instance, 20);
				}
			}
			
		});

	}
}
