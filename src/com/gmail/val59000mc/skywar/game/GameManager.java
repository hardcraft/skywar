package com.gmail.val59000mc.skywar.game;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import com.gmail.val59000mc.skywar.SkyWar;
import com.gmail.val59000mc.skywar.chests.ChestManager;
import com.gmail.val59000mc.skywar.classes.PlayerClassManager;
import com.gmail.val59000mc.skywar.commands.ChatCommand;
import com.gmail.val59000mc.skywar.commands.StartCommand;
import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.skywar.i18n.I18n;
import com.gmail.val59000mc.skywar.listeners.PingListener;
import com.gmail.val59000mc.skywar.listeners.PlayerChatListener;
import com.gmail.val59000mc.skywar.listeners.PlayerConnectionListener;
import com.gmail.val59000mc.skywar.listeners.PlayerDamageListener;
import com.gmail.val59000mc.skywar.listeners.PlayerDeathListener;
import com.gmail.val59000mc.skywar.maploader.MapLoader;
import com.gmail.val59000mc.skywar.players.PlayersManager;
import com.gmail.val59000mc.skywar.players.STeam;
import com.gmail.val59000mc.skywar.threads.CheckRemainingPlayerThread;
import com.gmail.val59000mc.skywar.threads.ChestRefillThread;
import com.gmail.val59000mc.skywar.threads.PreventFarAwayPlayerThread;
import com.gmail.val59000mc.skywar.threads.RestartThread;
import com.gmail.val59000mc.skywar.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.skywar.threads.WaitForNewPlayersThread;
import com.gmail.val59000mc.spigotutils.Logger;


public class GameManager {

	private static GameManager instance = null;
	
	private GameState state;
	private boolean pvp;
	
	private ChestManager chestManager;
	
	// static
	
	public static GameManager instance(){
		if(instance == null){
			instance = new GameManager();
		}
		return instance;
	}

	
	// constructor 
	
	private GameManager(){
		this.state = GameState.LOADING;
		this.pvp = false;
		this.chestManager = new ChestManager();
	}

	
	// accessors
	
	public GameState getState() {
		return state;
	}
	
	

	public ChestManager getChestManager() {
		return chestManager;
	}


	public boolean isPvp() {
		return pvp;
	}


	public boolean isState(GameState state) {
		return getState().equals(state);
	}
	
	// methods 
	
	public void loadGame() {
		state = GameState.LOADING;
		Logger.debug("-> GameManager::loadNewGame");
		Config.load();
		
		MapLoader.deleteOldPlayersFiles();
		MapLoader.load();
		
		PlayerClassManager.load();
		
		this.chestManager.load();
		this.chestManager.refillChests();
		
		registerCommands();
		registerListeners();
		
		if(Config.isBungeeEnabled)
			SkyWar.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(SkyWar.getPlugin(), "BungeeCord");

		Bukkit.getScheduler().runTaskLater(SkyWar.getPlugin(), new Runnable() {
			@Override
			public void run() {
				waitForNewPlayers();
			}
		}, 20);
		
		Logger.debug("<- GameManager::loadNewGame");
	}


	
	private void registerListeners(){
		Logger.debug("-> GameManager::registerListeners");
		// Registers Listeners
		List<Listener> listeners = new ArrayList<Listener>();		
		listeners.add(new PlayerConnectionListener());	
		listeners.add(new PlayerChatListener());
		listeners.add(new PlayerDamageListener());
		listeners.add(new PlayerDeathListener());
		listeners.add(new PingListener());
		for(Listener listener : listeners){
			Logger.debug("Registering listener="+listener.getClass().getSimpleName());
			Bukkit.getServer().getPluginManager().registerEvents(listener, SkyWar.getPlugin());
		}
		Logger.debug("<- GameManager::registerListeners");
	}
	
	private void registerCommands(){
		Logger.debug("-> GameManager::registerCommands");
		// Registers Listeners	
		SkyWar.getPlugin().getCommand("chat").setExecutor(new ChatCommand());
		SkyWar.getPlugin().getCommand("start").setExecutor(new StartCommand());
		Logger.debug("<- GameManager::registerCommands");
	}



	private void waitForNewPlayers(){
		Logger.debug("-> GameManager::waitForNewPlayers");
		WaitForNewPlayersThread.start();
		PreventFarAwayPlayerThread.start();
		UpdateScoreboardThread.start();
		state = GameState.WAITING;
		Logger.infoC(I18n.get("game.player-allowed-to-join"));
		Logger.debug("<- GameManager::waitForNewPlayers");
	}
	
	public void startGame(){
		Logger.debug("-> GameManager::startGame");
		state = GameState.STARTING;
		Logger.broadcast(I18n.get("game.start"));
		PlayersManager.instance().startAllPlayers();	
		chestManager.eraseLobby();
		playGame();
		Logger.debug("<- GameManager::startGame");
	}
	
	private void playGame(){
		state = GameState.PLAYING;
		pvp = true;
		CheckRemainingPlayerThread.start();
		ChestRefillThread.start();
	}
	
	public void endGame(EndCause cause, STeam winningTeam) {
		if(state.equals(GameState.PLAYING)){
			state = GameState.ENDED;
			pvp = false;
			CheckRemainingPlayerThread.stop();
			PreventFarAwayPlayerThread.stop();
			PlayersManager.instance().endAllPlayers(cause,winningTeam);
			RestartThread.start();
		}
		
	}

	
	


	
	
	

}
