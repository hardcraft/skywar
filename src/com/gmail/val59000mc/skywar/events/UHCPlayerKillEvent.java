package com.gmail.val59000mc.skywar.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.skywar.players.SPlayer;

public final class UHCPlayerKillEvent extends Event{
	
	private static final HandlerList handlers = new HandlerList();
	private SPlayer killer;
	private SPlayer killed;
	
	public UHCPlayerKillEvent(SPlayer killer, SPlayer killed){
		this.killed = killer;
		this.killed = killed;
	}
	
	public SPlayer getKiller(){
		return killer;
	}
	
	public SPlayer getKilled(){
		return killed;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
}
