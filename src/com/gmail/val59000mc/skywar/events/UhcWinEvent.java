package com.gmail.val59000mc.skywar.events;

import java.util.Set;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.skywar.players.SPlayer;

public class UhcWinEvent extends Event{

	private static final HandlerList handlers = new HandlerList();
	private Set<SPlayer> winners;
	
	public UhcWinEvent(Set<SPlayer> winners){
		this.winners = winners;
	}

	public Set<SPlayer> getWinners(){
		return winners;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

}
