package com.gmail.val59000mc.skywar.dependencies;

import org.bukkit.configuration.ConfigurationSection;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.actions.parsers.ActionParser;
import com.gmail.val59000mc.simpleinventorygui.exceptions.ActionParseException;

public class SelectTeamActionParser extends ActionParser{
	

	@Override
	public String getType() {
		return "select-team";
	}

	@Override
	public Action parseAction(ConfigurationSection action) throws ActionParseException {
		String team = action.getString("team");
		if(team == null){
			throw new ActionParseException("#The "+getType()+" action "+action.getName()+" must provide a team attribute");
		}
		return new SelectTeamAction(team);
	}
}
