package com.gmail.val59000mc.skywar.dependencies;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.skywar.game.GameState;
import com.gmail.val59000mc.skywar.players.PlayersManager;
import com.gmail.val59000mc.skywar.players.SPlayer;
import com.gmail.val59000mc.skywar.players.STeam;
import com.gmail.val59000mc.skywar.players.TeamType;
import com.gmail.val59000mc.spigotutils.Sounds;

public class SelectTeamAction extends Action{
	
	private String team;
	
	public SelectTeamAction(String team){
		this.team = team;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {

		if(team.equals("leave")){
			leaveTeam(player,sigPlayer);
		}else{
			selectTeam(player,sigPlayer);
		}
		
		
	}

	private void selectTeam(Player player, SigPlayer sigPlayer) {
		
		TeamType type = TeamType.valueOf(team);
		
		if(GameManager.instance().isState(GameState.WAITING)){

			PlayersManager pm = PlayersManager.instance();
			SPlayer uPlayer = pm.getSPlayer(player);
			
			if(uPlayer != null){
				STeam oldTeam = uPlayer.getTeam();
				uPlayer.leaveTeam();
				
				STeam newTeam = PlayersManager.instance().getTeam(type);
				
				if(newTeam.isFull()){
					if(oldTeam != null){
						oldTeam.addPlayer(uPlayer);
					}
					uPlayer.sendI18nMessage("team.full");
					Sounds.play(player, Sound.VILLAGER_NO, 1, 2);
					executeNextAction(player, sigPlayer, false);
					return;
				}else{
					newTeam.addPlayer(uPlayer);
					uPlayer.refreshNameTag();
					executeNextAction(player, sigPlayer, true);
					return;
				}
			}
		}
		
		executeNextAction(player, sigPlayer, false);
	}
	

	private void leaveTeam(Player player, SigPlayer sigPlayer) {
		
		if(GameManager.instance().isState(GameState.WAITING)){

			PlayersManager pm = PlayersManager.instance();
			SPlayer uPlayer = pm.getSPlayer(player);
			
			if(uPlayer != null){
				uPlayer.leaveTeam();
				uPlayer.refreshNameTag();
				executeNextAction(player, sigPlayer, true);
			}
		}
		
		executeNextAction(player, sigPlayer, false);
	}
}
