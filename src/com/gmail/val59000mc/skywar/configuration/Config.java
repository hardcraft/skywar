package com.gmail.val59000mc.skywar.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.api.SIGApi;
import com.gmail.val59000mc.simpleinventorygui.exceptions.InventoryParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.TriggerParseException;
import com.gmail.val59000mc.skywar.SkyWar;
import com.gmail.val59000mc.skywar.dependencies.SelectTeamActionParser;
import com.gmail.val59000mc.skywar.dependencies.TeamPlaceholder;
import com.gmail.val59000mc.skywar.dependencies.VaultManager;
import com.gmail.val59000mc.skywar.i18n.I18n;
import com.gmail.val59000mc.skywar.maploader.MapName;
import com.gmail.val59000mc.skywar.players.TeamType;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.google.common.collect.Lists;

import net.milkbowl.vault.Vault;

public class Config {
	
	// Dependencies
	public static boolean isVaultLoaded;
	public static boolean isBountifulApiLoaded;
	public static boolean isSIGLoaded;
	
	// World 
	public static String lastWorldName;
	public static String worldName;
	public static boolean isLoadLastWord;
	public static String mapName;

	// Players
	public static int playersToStart;
	public static int countdownToStart;
	public static int maxPlayers;
	public static int playersPerTeam;
	
	// Locations
	public static Location lobby;
	public static int lobbyRadius;
	public static Location center;
	public static int centerRadius;
	public static List<Location> spawnsLocations;
	public static int spawnsRadius;
	public static LocationBounds bounds;
	
	
	// Items
	public static List<ItemStack> spawnsItems;
	public static List<ItemStack> centerItems;
	public static int refillChestsDelay;
	
	// Bungee
	public static boolean isBungeeEnabled;
	public static String bungeeServer;
	
	// Rewards
	public static double killReward;
	public static double winReward;
	public static double objectiveReward;
	public static double vipRewardBonus;
	public static double vipPlusRewardBonus;
	public static double helperRewardBonus;
	public static double builderRewardBonus;
	public static double moderateurRewardBonus;
	public static double administrateurRewardBonus;
	
	public static void load(){
		
		FileConfiguration cfg = SkyWar.getPlugin().getConfig();

		// Debug
		Logger.setDebug(cfg.getBoolean("debug",false));
		
		Logger.debug("-> Config::load");
		// World
		lastWorldName = cfg.getString("world.last-world","last_wic_world");
		isLoadLastWord = cfg.getBoolean("world.load-last-world",true);
		worldName = UUID.randomUUID().toString();
		mapName = MapName.values()[Randoms.randomInteger(0, MapName.values().length-1)].toString();
		Logger.debug("Choosing map : "+mapName);
		
		// Items
		spawnsItems = Lists.newArrayList();
		for(String itemStr : cfg.getStringList("stuff.spawn")){
			spawnsItems.add(Parser.parseItemStack(itemStr));
		}
		
		centerItems = Lists.newArrayList();
		for(String itemStr : cfg.getStringList("stuff.center")){
			centerItems.add(Parser.parseItemStack(itemStr));
		}
		
		refillChestsDelay = cfg.getInt("refill-chests-delay",300);
		
		// Players
		playersToStart = cfg.getInt("players."+mapName+".min",10);
		countdownToStart = cfg.getInt("players."+mapName+".countdown",10);
		maxPlayers = cfg.getInt("players."+mapName+".max",20);
		playersPerTeam = cfg.getInt("players."+mapName+".per-team",2);
		
		// Bungee
		isBungeeEnabled = cfg.getBoolean("bungee.enable",false);
		bungeeServer = cfg.getString("bungee.server","lobby");
		
		// Rewards
		killReward = cfg.getDouble("reward.kill",5);
		winReward = cfg.getDouble("reward.win",50);
		objectiveReward = cfg.getDouble("reward.objective",15);
		vipRewardBonus = cfg.getDouble("reward.vip",100);
		vipPlusRewardBonus = cfg.getDouble("reward.vip+",100);
		helperRewardBonus = cfg.getDouble("reward.helper",100);
		builderRewardBonus = cfg.getDouble("reward.builder",100);
		moderateurRewardBonus = cfg.getDouble("reward.moderateur",100);
		administrateurRewardBonus = cfg.getDouble("reward.administrateur",100);
		
		// Dependencies
		loadVault();
		loadSIG();
		loadBountifulApi();
		
		// Sig
		if(isSIGLoaded){
			
			SIGApi sig = SIG.getPlugin().getAPI();
			sig.registerActionParser(new SelectTeamActionParser());
			sig.registerPlaceholder(new TeamPlaceholder("{members.RED}",TeamType.RED));
			sig.registerPlaceholder(new TeamPlaceholder("{members.AQUA}",TeamType.AQUA));
			sig.registerPlaceholder(new TeamPlaceholder("{members.BLUE}",TeamType.BLUE));
			sig.registerPlaceholder(new TeamPlaceholder("{members.DARK_PURPLE}",TeamType.DARK_PURPLE));
			sig.registerPlaceholder(new TeamPlaceholder("{members.GOLD}",TeamType.GOLD));
			sig.registerPlaceholder(new TeamPlaceholder("{members.GREEN}",TeamType.GREEN));
			sig.registerPlaceholder(new TeamPlaceholder("{members.LIGHT_PURPLE}",TeamType.LIGHT_PURPLE));
			sig.registerPlaceholder(new TeamPlaceholder("{members.YELLOW}",TeamType.YELLOW));
			
			try {
				sig.registerConfigurationFile(new File(SkyWar.getPlugin().getDataFolder(), "teams.yml"));
			} catch (InventoryParseException | TriggerParseException e) {
				//
			}
			
		}
		
		VaultManager.setupEconomy();
		Logger.debug("<- Config::load");
	}
	
	private static void loadVault(){
		Logger.debug("-> Config::loadVault");
		
		Plugin vault = Bukkit.getPluginManager().getPlugin("Vault");
        if(vault == null || !(vault instanceof Vault)) {
            Logger.warn(I18n.get("config.dependency.vault-not-found"));
        	 isVaultLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.vault-loaded"));
        	 isVaultLoaded = true;
        }
		Logger.debug("<- Config::loadVault");
	}
	
	private static void loadBountifulApi(){
		Logger.debug("-> Config::loadBountifulApi");
		
		Plugin bountifulApi = Bukkit.getPluginManager().getPlugin("BountifulAPI");
        if(bountifulApi == null || !(bountifulApi instanceof BountifulAPI)) {
            Logger.warn(I18n.get("config.dependency.bountiful-not-found"));
        	 isBountifulApiLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.bountifulapi-loaded"));
            isBountifulApiLoaded = true;
        }
		Logger.debug("<- Config::loadBountifulApi");
	}

	private static void loadSIG(){
		Logger.debug("-> Config::loadSIG");
		
		Plugin sig = Bukkit.getPluginManager().getPlugin("SimpleInventoryGUI");
        if(sig == null || !(sig instanceof SIG)) {
            Logger.warn(I18n.get("config.dependency.sig-not-found"));
        	 isSIGLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.sig-loaded"));
            isSIGLoaded = true;
        }
		Logger.debug("<- Config::loadSIG");
	}


	public static void loadWorldLocations() {
		Logger.debug("-> Config::loadWorldLocations");
		
		FileConfiguration cfg = SkyWar.getPlugin().getConfig();
		
		World world = Bukkit.getWorld(worldName);
		
		lobby = Parser.parseLocation(world,cfg.getString("locations."+mapName+".lobby","0 100 0 0 0"));
		lobbyRadius = cfg.getInt("locations."+mapName+".lobby-radius");
		
		center = Parser.parseLocation(world,cfg.getString("locations."+mapName+".center","0 100 0 0 0"));
		centerRadius = cfg.getInt("locations."+mapName+".center-radius");
		
		spawnsLocations = new ArrayList<Location>();
		for(String locStr : cfg.getStringList("locations."+mapName+".spawns")){
			spawnsLocations.add(Parser.parseLocation(world,locStr));
		}
		spawnsRadius = cfg.getInt("locations."+mapName+".spawns-radius");
		Collections.shuffle(spawnsLocations);
		// duplicate last location if size < 8
		while(spawnsLocations.size() < 8){
			spawnsLocations.add(spawnsLocations.get(spawnsLocations.size()-1).clone());
		}
		
		
		String minBoundsStr = cfg.getString("locations."+mapName+".bounds.min","-55 90 55 0 0");
		String maxBoundsStr = cfg.getString("locations."+mapName+".bounds.max","55 58 -55 0 0");
		bounds = new LocationBounds(Parser.parseLocation(world,minBoundsStr), Parser.parseLocation(world,maxBoundsStr));
		
		Logger.debug("<- Config::loadWorldLocations");
		
	}
		
		
		
		
}
