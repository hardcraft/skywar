package com.gmail.val59000mc.skywar.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.gmail.val59000mc.skywar.SkyWar;
import com.gmail.val59000mc.skywar.configuration.Config;
import com.gmail.val59000mc.skywar.dependencies.VaultManager;
import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.skywar.game.GameState;
import com.gmail.val59000mc.skywar.i18n.I18n;
import com.gmail.val59000mc.skywar.players.PlayerState;
import com.gmail.val59000mc.skywar.players.PlayersManager;
import com.gmail.val59000mc.skywar.players.SPlayer;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Texts;

public class PlayerDeathListener implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(final PlayerDeathEvent event) {

		SPlayer wicPlayer = PlayersManager.instance().getSPlayer(event.getEntity());
		
		if(wicPlayer == null || !wicPlayer.isOnline()){
			return;
		}
		
		handleDeathPlayer(event,wicPlayer);
		
	}
	
	public void handleDeathPlayer(final PlayerDeathEvent event, SPlayer dead){
		PlayersManager pm = PlayersManager.instance();
		
		// Add death score to dead player and kill score to killer if playing
		if(GameManager.instance().isState(GameState.PLAYING)){
			
			SPlayer killer = (dead.getPlayer().getKiller() == null) ? null : pm.getSPlayer(dead.getPlayer().getKiller());
			
			event.setDeathMessage(I18n.get("player.died").replace("%player%",dead.getColor()+dead.getName()));
			
			if(killer != null && killer.isOnline()){
				killer.addKill();
				rewardKill(killer);
				
				event.setDeathMessage(
						I18n.get("player.killed")
						.replace("%killer%",killer.getColor()+killer.getName())
						.replace("%killed%",dead.getColor()+dead.getName())
				);
			}
			
			dead.setState(PlayerState.DEAD);
			
			if(dead.isOnline() && PlayersManager.instance().getPlayingTeams().size() > 1){

				Texts.tellraw(dead.getPlayer(), I18n.get("stats.death",dead.getPlayer())
					.replace("%kills%", String.valueOf(dead.getKills()))
					.replace("%hardcoins%", String.valueOf(dead.getMoney()))
				);
				
			}
			
			Bukkit.getScheduler().runTaskLater(SkyWar.getPlugin(), new Runnable() {
				
				public void run() {
			        event.getEntity().spigot().respawn();
				}
			}, 20);
		}
		
	}

	private void rewardKill(SPlayer killer) {
		if(killer.isOnline()){
			Player player = killer.getPlayer();
			double money = VaultManager.addMoney(player, Config.killReward);
			killer.addMoney(money);
			player.sendMessage("§a+"+money+" §fHC");
			Sounds.play(player, Sound.LEVEL_UP, 1, 2);
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Logger.debug("-> PlayerDeathListener::onPlayerRespawn, player="+event.getPlayer().getName());
		
		SPlayer wicPlayer = PlayersManager.instance().getSPlayer(event.getPlayer());
		
		if(wicPlayer == null){
			return;
		}
		
		handleRespawnPlayer(event,wicPlayer);
		
		Logger.debug("<- PlayerDeathListener::onPlayerRespawn");
	}
	
	public void handleRespawnPlayer(PlayerRespawnEvent event, SPlayer wicPlayer){
		Logger.debug("-> PlayerDeathListener::handleRespawnPlayer, player="+wicPlayer);

		final String name = wicPlayer.getName();
		
		switch(GameManager.instance().getState()){
			case WAITING:
					Bukkit.getScheduler().runTaskLater(SkyWar.getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						Logger.debug("waitPlayer");
						SPlayer p = PlayersManager.instance().getSPlayer(name);
						if(p != null){
							PlayersManager.instance().waitPlayer(p);
						}
						
					}
				}, 1);
				break;
			case LOADING:
			case STARTING:
			case ENDED:
			default:
			case PLAYING:
				Bukkit.getScheduler().runTaskLater(SkyWar.getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						Logger.debug("teamSpawnPlayer");
						SPlayer p = PlayersManager.instance().getSPlayer(name);
						if(p != null){
							PlayersManager.instance().spectatePlayer(p);
						}
						
					}
				}, 1);
				break;
		}
		

		Logger.debug("<- PlayerDeathListener::handleRespawnPlayer");
	}
	
}
