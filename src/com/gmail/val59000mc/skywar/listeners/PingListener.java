package com.gmail.val59000mc.skywar.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.skywar.i18n.I18n;

public class PingListener implements Listener{
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPing(ServerListPingEvent event){
		GameManager gm = GameManager.instance();
		switch(gm.getState()){
			case LOADING:
				event.setMotd(I18n.get("ping.loading"));
				break;
			case PLAYING:
				event.setMotd(I18n.get("ping.playing"));
				break;
			case STARTING:
				event.setMotd(I18n.get("ping.starting"));
				break;
			case WAITING:
				event.setMotd(I18n.get("ping.waiting"));
				break;
			case ENDED:
				event.setMotd(I18n.get("ping.ended"));
				break;
			default:
				event.setMotd(I18n.get("ping.loading"));
				break;
		
		}
	}
}
