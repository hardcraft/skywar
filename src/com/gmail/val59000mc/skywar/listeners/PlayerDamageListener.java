package com.gmail.val59000mc.skywar.listeners;

import java.util.Collection;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.skywar.game.GameManager;
import com.gmail.val59000mc.skywar.players.PlayerState;
import com.gmail.val59000mc.skywar.players.PlayersManager;
import com.gmail.val59000mc.skywar.players.SPlayer;

public class PlayerDamageListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageByEntityEvent event){
		handleFriendlyFire(event);
		handleProjectile(event);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event){
		handleAnyDamage(event);
	}


	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPotionSplash(PotionSplashEvent event){
		handlePotionSplash(event);
	}
	
	
	///////////////////////
	// PotionSplashEvent //
	///////////////////////
	
	private void handlePotionSplash(PotionSplashEvent event) {
		if(event.getEntity().getShooter() instanceof Player){
			PlayersManager pm = PlayersManager.instance();
			
			Player damager = (Player) event.getEntity().getShooter();
			SPlayer tDamager = pm.getSPlayer(damager);
			
			if(tDamager != null){
			
				if(isAttackPotion(event.getPotion())){
					// Cancelling potion damage for teamates
					for(LivingEntity living : event.getAffectedEntities()){
						if(living instanceof Player){
							Player damaged = (Player) living;
							SPlayer tDamaged = pm.getSPlayer(damaged);
							if(tDamager.isInTeamWith(tDamaged)){
								event.setIntensity(living, 0);
							}
						}
					}
				}
				
			}
		}
	
	}
	
	// Only checking the first potion effect, considering vanilla potions
	private boolean isAttackPotion(ThrownPotion potion){
		Collection<PotionEffect> effects = potion.getEffects();
		if(effects.size() > 0){
			PotionEffectType effect = effects.iterator().next().getType();
			return ( 
				effect.equals(PotionEffectType.HARM) ||
			    effect.equals(PotionEffectType.POISON) ||
			    effect.equals(PotionEffectType.WEAKNESS) ||
				effect.equals(PotionEffectType.SLOW) ||
			    effect.equals(PotionEffectType.SLOW_DIGGING) ||
			    effect.equals(PotionEffectType.CONFUSION) ||
			    effect.equals(PotionEffectType.BLINDNESS) ||
			    effect.equals(PotionEffectType.HUNGER) ||
			    effect.equals(PotionEffectType.WITHER)
			  );
		}
		return false;
	}

	
	///////////////////////
	// EntityDamageEvent //
	///////////////////////
	
	private void handleAnyDamage(EntityDamageEvent event){
		if(event.getEntity() instanceof Player){
			SPlayer wicPlayer = PlayersManager.instance().getSPlayer((Player) event.getEntity());
			if(wicPlayer != null && wicPlayer.getState().equals(PlayerState.WAITING)){
				event.setCancelled(true);
			}
		}
	}
	
	///////////////////////////////
	// EntityDamageByEntityEvent //
	///////////////////////////////
	
	private void handleFriendlyFire(EntityDamageByEntityEvent event){

		PlayersManager pm = PlayersManager.instance();
		
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player){
			
			Player damager = (Player) event.getDamager();
			Player damaged = (Player) event.getEntity();
			
			SPlayer wicDamager = pm.getSPlayer(damager);
			SPlayer wicDamaged = pm.getSPlayer(damaged);
			
			if(wicDamaged != null && wicDamager != null){
				if(wicDamaged.isInTeamWith(wicDamager)){
					event.setCancelled(true);
				}
			}
		}
	}

	private void handleProjectile(EntityDamageByEntityEvent event){

		if(!event.isCancelled()){
			PlayersManager pm = PlayersManager.instance();
			GameManager gm = GameManager.instance();
			
			
			if(event.getEntity() instanceof Player && event.getDamager() instanceof Projectile){
				Projectile projectile = (Projectile) event.getDamager();
				final Player shot = (Player) event.getEntity();
				if(projectile.getShooter() instanceof Player){
					
					if(!gm.isPvp()){
						event.setCancelled(true);
						return;
					}
					
					final Player shooter = (Player) projectile.getShooter();
					SPlayer sDamager = pm.getSPlayer(shooter);
					SPlayer sDamaged = pm.getSPlayer(shot);

					if(sDamager != null && sDamaged != null){
						if(sDamager.getState().equals(PlayerState.PLAYING) && sDamager.isInTeamWith(sDamaged)){
							event.setCancelled(true);
						}
					}
				}
			}
		}
		
	}
}
